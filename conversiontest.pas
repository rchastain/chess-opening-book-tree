
uses
  SysUtils, Classes, TreeList, IdenticalFiles;

procedure Test(const AOriginalFile, ATempFile: TFileName);
var
  LTreeList: TTreeList;
  LFile0, LFile1, LFile2: TFileName;
begin
  if not FileExists(AOriginalFile) then
  begin
    WriteLn('File not found: ', AOriginalFile);
    Exit;
  end;
  
  LFile0 := Format(ATempFile, [0]);
  LFile1 := Format(ATempFile, [1]);
  LFile2 := Format(ATempFile, [2]);
  
  LTreeList := TTreeList.Create;
  LTreeList.LoadFromFile(AOriginalFile);
  LTreeList.SaveToFile(LFile0);
  LTreeList.Free;
  
  LTreeList := TTreeList.Create;
  LTreeList.LoadFromFile(LFile0);
  LTreeList.SaveToFileCompact(LFile1);
  LTreeList.Free;
  
  LTreeList := TTreeList.Create;
  LTreeList.LoadFromFileCompact(LFile1);
  LTreeList.SaveToFile(LFile2);
  LTreeList.Free;
  
  WriteLn('Original file: ', AOriginalFile);
  WriteLn('  Comparison 1: ', CompareFiles(LFile0, AOriginalFile));
  WriteLn('  Comparison 2: ', CompareFiles(LFile0, LFile2));
end;

begin
  CreateDir('temp');
  Test('alouette/white.txt', 'temp/alouette-white-%d.txt');
  Test('alouette/black.txt', 'temp/alouette-black-%d.txt');
  Test('aristarch/white.txt', 'temp/aristarch-white-%d.txt');
  Test('aristarch/black.txt', 'temp/aristarch-black-%d.txt');
  Test('cassandre/white.txt', 'temp/cassandre-white-%d.txt');
  Test('cassandre/black.txt', 'temp/cassandre-black-%d.txt');
  Test('cpw-engine/book.txt', 'temp/cpw-engine-%d.txt');
  Test('ladameblanche/book.txt', 'temp/ladameblanche-%d.txt');
  Test('tscp/book.txt', 'temp/tscp-%d.txt');
end.
