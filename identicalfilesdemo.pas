
uses
  SysUtils, IdenticalFiles;

procedure Test(const AComparisonMode: TComparisonMode; const AFile1, AFile2: TFileName);
var
  LStart: cardinal;
begin
  WriteLn(AComparisonMode);
  LStart := GetTickCount64;
  WriteLn(CompareFiles(AFile1, AFile2, AComparisonMode));
  WriteLn(GetTickCount64 - LStart, ' ms');
end;

var
  m: TComparisonMode;
  
begin
  if FileExists(ParamStr(1)) and FileExists(ParamStr(2)) then
    for m := Low(TComparisonMode) to High(TComparisonMode) do
      Test(m, ParamStr(1), ParamStr(2));
end.
