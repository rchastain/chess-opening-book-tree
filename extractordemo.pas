
uses
  SysUtils, Classes, Extractor;
  
const
  CCompactBook = 'ladameblanche/book';

var
  LFile: TStringList;
  LLines: TStringListArray;
  i: integer;
  
begin
  LFile := TStringList.Create;
  try
    LFile.LoadFromFile(CCompactBook);
    if LFile.Count >= 1 then
    begin
      
      LLines := Extractor.ExtractLines(LFile[0]);
      
      for i := 0 to High(LLines) do
      begin
        WriteLn(LLines[i].DelimitedText);
        LLines[i].Free;
      end;
      
      SetLength(LLines, 0);
      
    end;
  except
    WriteLn('Zut');
  end;
  LFile.Free;
end.
