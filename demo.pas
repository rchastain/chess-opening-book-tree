
uses
  SysUtils, Classes, TreeList;

const
  CBook: array[0..2] of string = (
    'e2e4 e7e5 d2d4',
    'e2e4 c7c5',
    'd2d4 d7d5'
  );

var
  LTreeList: TTreeList;
  i: integer;
  
begin
  LTreeList := TTreeList.Create;
  
  for i := 0 to 2 do
    LTreeList.AddLine(CBook[i]);
  
  WriteLn('"', LTreeList.FindChild('',          FALSE), '"');
  WriteLn('"', LTreeList.FindChild('e2e4',      FALSE), '"');
  WriteLn('"', LTreeList.FindChild('e2e4 e7e5', FALSE), '"');
  
  LTreeList.SaveToFile('demo-book.txt');
  LTreeList.SaveToFileCompact('demo-book-compact.txt');
  
  LTreeList.Free;
end.
