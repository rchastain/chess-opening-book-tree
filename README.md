# Chess Opening Book Tree

## Overview

This is a collection of Pascal units for creation and usage of a chess opening book.

These units are used by the chess engine [Alouette](https://gitlab.com/rchastain/alouette).

## File formats

Two file formats are supported.

The "lines" format.

```
e2e4 e7e5 d2d4
e2e4 c7c5
d2d4 d7d5
```

The "compact" format.

```
(e2e4(e7e5(d2d4))(c7c5))(d2d4(d7d5))
```

## History

The compact format is the format described by [Kathe Spracklen](https://content.iospress.com/articles/icga-journal/icg6-1-04).

This format has been previously used by Marc-Philippe Huget in his chess engine, [La Dame Blanche](http://www.quarkchess.de/ladameblanche/).
